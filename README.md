# vue-leaflet
Vue project to implement maps using leaflet.


## Project setup
```
npm install
```

### Install leaflet
```
npm install vue2-leaflet leaflet --save
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

